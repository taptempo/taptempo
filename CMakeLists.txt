#    TapTempo, a command line tap tempo.
#    Copyright (C) 2017 Francois Mazen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


cmake_minimum_required (VERSION 3.1)

set (CMAKE_CXX_STANDARD 11)

set(TAPTEMPO_VERSION_MAJOR 1)
set(TAPTEMPO_VERSION_MINOR 4)
set(TAPTEMPO_VERSION_REVISION 5)
set(TAPTEMPO_PACKAGE_NAME taptempo)

project (${TAPTEMPO_PACKAGE_NAME})

if(MSYS)
	set(TAPTEMPO_LOCALE_DIR "share/locale")
	set(TAPTEMPO_INSTALL_DIR ${CMAKE_INSTALL_PREFIX})
else()
	set(TAPTEMPO_LOCALE_DIR "${CMAKE_INSTALL_PREFIX}/share/locale")
	set(TAPTEMPO_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/bin)
endif()

configure_file (
        "${PROJECT_SOURCE_DIR}/src/taptempo_config.h.in"
        "${PROJECT_BINARY_DIR}/taptempo_config.h"
        )
include_directories("${PROJECT_BINARY_DIR}")

add_subdirectory(src)
add_subdirectory(po)

# Create test target.

if(${CMAKE_VERSION} VERSION_LESS "3.11.0" AND ${CMAKE_VERSION} VERSION_GREATER "3.0.0")
    # Disable error report when creating "test" target.
    # This happens with CMake version between 3.0 and 3.11
    cmake_policy(SET CMP0037 OLD)
endif()

add_custom_target(test)
add_subdirectory(test)


